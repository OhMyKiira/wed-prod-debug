import axios from 'axios'

const API = axios.create({ 
    baseURL : 'http://localhost:8000/api/v1/'
})

const GetAllKehadiran = () => API.get('/kehadiran')
const PostKehadiran = data => API.post(`/post/kehadiran` , data)

export const APIS = { 
    GetAllKehadiran,
    PostKehadiran
}

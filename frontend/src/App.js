import { useForm } from 'react-hook-form'

import { APIS } from './api/index'

function App() {
  const { register, handleSubmit, reset } = useForm()

  const PostKehadiran = async (data) => {
     try {
      await APIS.PostKehadiran(data)
      console.log(data)
      reset()
     } catch (error) {
        console.log(error)
     }
  }

  return (
    <>
      <form onSubmit={handleSubmit(PostKehadiran)}>
        <div>
          <label htmlFor="nama">Isi Nama</label>
          <input type="text" {...register('nama', { required: true })} />
        </div>
        <br />
        <div>
          <label htmlFor="pesan">Isi Pesan</label>
          <input type="text" {...register('pesan', { required: true })} />
        </div>
        <br />
        <div>
          <label htmlFor="kehadiran">Konfiirmasi</label>
          <select name="kehadiran" {...register('kehadiran')}>
            <option value=""></option>
            <option value="hadir">Hadir</option>
            <option value="tidak hadir">Tidak hadir</option>
          </select>
        </div>
        <input type="submit" value="Konfirmasi" />
      </form>
    </>
  );
}

export default App;

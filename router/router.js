const express = require('express');
const router = express.Router();

const KehadiranController = require('../controller/kehadiranController')

// ----------------------------------
// Router
// ----------------------------------
router.get('/' , (req , res) => { 
    res.send('Hello World')
})

// ----------------------------------
// Controller Router
// ----------------------------------
router.get('/kehadiran' , KehadiranController.GetAllKehadiran)
router.post('/post/kehadiran' , KehadiranController.PostKehadiran)

module.exports = router
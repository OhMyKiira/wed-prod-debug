'use-strict'

const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const cors = require('cors')
const PORT = 8000
const morgan = require('morgan')
const dotenv = require('dotenv')
const path = require('path')

const connectDB = require('./config/db')
const router = require('./router/router')

// ----------------------------------
// Express configuration
// ----------------------------------
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cors({origin: '*' }))
// app.options("", cors());
// app.use(function (req, res, next) 
// {
//       res.header("Access-Control-Allow-Origin", "*");
//       res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
//       res.header("Access-Control-Allow-Headers","Origin,X-Requested-With, X-CallbackType, Content-Type, Accept");
//       res.header("Cache-Control", "no-cache");
//       if ("OPTIONS" == req.method) 
//       {
//           res.send(200);
//       }
//       else 
//      {
//        next();
//      }
// });

if (process.env.NODE_ENV == 'development') {
    app.use(morgan('dev'))
}

// ----------------------------------
// Api Routes
// ----------------------------------
app.use('/api/v1', router)


// ----------------------------------
// DB Config
// ----------------------------------
dotenv.config({ path : '.env'})
connectDB()

// ----------------------------------
// Deployment Config
// ----------------------------------
if (process.env.NODE_ENV == 'production') {
    app.use(express.static(path.join(__dirname, 'frontend', 'build')))
    app.get("/", (_, res) => {
        res.sendFile(
            path.join(__dirname, './frontend/build/index.html'),
            function (err) {
                err && res.status(500).send(err)
            }
        )
    })
}
// ----------------------------------
// Express PORT server
// ----------------------------------
app.listen(PORT, () => {
    console.log(`Server running on ${PORT}`)
})

module.exports = app